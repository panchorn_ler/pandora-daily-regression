let db = connect("localhost:27017/result");

db.createUser({
    user: "bot",
    pwd: "helloW0rd99",
    roles: [
        { role: "readWrite", db: "result" },
    ],
});

db.createCollection("daily-result");

db.getCollection("daily-result").insertMany([
    {
        "component": "recurring-service",
        "date": "2021-09-05",
        "env": "AWS",
        "result": "SUCCESS",
        "score": "100/100",
        "team": "Canopus"
    },
    {
        "component": "recurring-data-service",
        "date": "2021-09-05",
        "env": "AWS",
        "result": "SUCCESS",
        "score": "100/100",
        "team": "Canopus"
    }
]);
