import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MONGO_CONNECTION, MONGO_PASS, MONGO_USER } from './common/constants';
import { DailyResultModule } from './daily-result/daily-result.module';

@Module({
  imports: [
    MongooseModule.forRoot(MONGO_CONNECTION, { auth: { username: MONGO_USER, password: MONGO_PASS } }),
    DailyResultModule
  ],
})
export class AppModule { }
