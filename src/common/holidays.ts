
export function isHoliday(date) {
    // Ascend Holiday
    var array = ["2021-10-13", "2021-10-25", "2021-12-06", "2021-12-31"];
    return (array.find(item => { return item == date }) || []).length > 0;
}
