import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type DailyResultDocument = DailyResult & Document;

@Schema({ timestamps: true, collection: "daily-result" })
export class DailyResult {
  @Prop()
  public component!: string;

  @Prop()
  public result!: string;

  @Prop()
  public team!: string;

  @Prop()
  public score!: string;

  @Prop()
  public env!: string;

  @Prop()
  public date!: string;
}

export const DailyResultSchema = SchemaFactory.createForClass(DailyResult);