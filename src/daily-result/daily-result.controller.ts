import { Body, Controller, Get, HttpCode, HttpStatus, Param, Post, Query, Req, Res, } from '@nestjs/common';
import { DailyResult } from './daily-result.schema';
import { DailyResultService } from './daily-result.service'

@Controller('daily-result')
export class DailyResultController {
  constructor(private readonly dailyResultService: DailyResultService) { }

  @Get()
  async getDailyResult(
    @Query('date') date: string,
    @Res() res,
  ) {
    res.json({ "data": await this.dailyResultService.getDailyResult(date) })
  }

  @Post()
  @HttpCode(HttpStatus.OK)
  async insertDailyResult(
    @Body() body: DailyResult,
  ) {
    await this.dailyResultService.insertDailyResult(body)
  }

  @Get("notify")
  @HttpCode(HttpStatus.OK)
  async notifyDailyResult(
    @Query('date') date: string,
    @Res() res,
  ) {
    res.json({ "data": await this.dailyResultService.notifyDailyResult(date) })
  }

}
