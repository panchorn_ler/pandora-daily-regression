import { Module } from '@nestjs/common';
import { DailyResultController } from './daily-result.controller';
import { DailyResultService } from './daily-result.service';
import { MongooseModule } from '@nestjs/mongoose';
import { DailyResult, DailyResultSchema } from './daily-result.schema';

@Module({
  controllers: [DailyResultController],
  providers: [DailyResultService],
  imports: [
    MongooseModule.forFeature([{ name: DailyResult.name, schema: DailyResultSchema }]),
  ],
})
export class DailyResultModule { }
