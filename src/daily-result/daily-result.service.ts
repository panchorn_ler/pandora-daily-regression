import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { RESULT_ICON } from 'src/common/constants';
import { DailyResult, DailyResultDocument } from './daily-result.schema';
import * as util from 'util';
import * as moment from 'moment-timezone';
import { isHoliday } from 'src/common/holidays';

@Injectable()
export class DailyResultService {
    constructor(
        @InjectModel(DailyResult.name) private dailyResultsModel: Model<DailyResultDocument>,
    ) { }

    async insertDailyResult(dailyResult: DailyResult) {
        try {
            await this.dailyResultsModel.updateOne(
                { date: dailyResult.date, component: dailyResult.component },
                dailyResult,
                { upsert: true }
            )
        } catch (error) {
            console.log(error)
            throw new InternalServerErrorException(error);
        } finally {
            if (!isHoliday(dailyResult.date) && dailyResult.result === 'FAILURE') {
                // todo send noti
                const messageNotify = util.format(
                    "🆘 : %s\n🆘 : %s\n🆘 : passed %s",
                    dailyResult.component,
                    dailyResult.team,
                    dailyResult.score
                );
                console.log(messageNotify)
            }
        }
    }

    async getDailyResult(date: string): Promise<DailyResult[]> {
        const selectedDate = this.getDate(date);
        return await this.find(selectedDate);
    }

    async notifyDailyResult(date: string) {
        const selectedDate = this.getDate(date);
        const dailyResults = await this.find(selectedDate);
        const resultDict = this.initResultDict(selectedDate);
        dailyResults.forEach(data => {
            const notifyMessage = util.format("\n%s %s", RESULT_ICON[data.result], data.component);
            resultDict[data.team] = resultDict[data.team] + notifyMessage;
        });
        if (!isHoliday(selectedDate)) {
            // todo send noti
            for (const data in resultDict) {
                console.log(resultDict[data])
            }
        }
        return resultDict
    }

    private async find(date: string): Promise<DailyResult[]> {
        try {
            return await this.dailyResultsModel.find({ date: date }).sort({ team: 1, component: 1 });
        } catch (error) {
            console.log(error)
            throw new InternalServerErrorException(error);
        }
    }

    private getDate(date: string): string {
        return date ? date : moment.tz('Asia/Bangkok').format("YYYY-MM-DD")
    }

    private initResultDict(date: string) {
        return {
            'Vela': util.format("Summary: %s\nTeam: Vela", date),
            'Puppis': util.format("Summary: %s\nTeam: Puppis", date),
            'Canopus': util.format("Summary: %s\nTeam: Canopus", date),
            'Venus': util.format("Summary: %s\nTeam: Venus", date),
            '': util.format("Summary: %s\nTeam: <n/a>", date),
        }
    }
}
